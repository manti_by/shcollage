# SHcollage

[![CircleCI](https://img.shields.io/circleci/project/bitbucket/manti_by/shcollage/master.svg)](https://circleci.com/bb/manti_by/shcollage)
[![Docker](https://img.shields.io/docker/cloud/build/mantiby/shcollage.svg)](https://hub.docker.com/r/mantiby/shcollage/)

## About  

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)
[![License](https://img.shields.io/badge/license-BSD-blue.svg)](https://bitbucket.org/manti_by/shcollage/raw/5ef82a6d6031c47204a8bdab44dfa3ad5b997407/LICENSE)  

Author: Alexander Chaika <manti.by@gmail.com>

Source link: https://bitbucket.org/manti_by/shcollage/

Requirements: Docker, Python 3.7+, PostgreSQL


## Setup development environment

1. Install, create and activate virtualenv

        $ sudo pip install virtualenv
        $ virtualenv -p python3 --no-site-packages --prompt=shc- venv
        $ source venv/bin/activate

2. Clone sources and install pip packages

        $ mkdir shcollage/ && cd shcollage/
        $ git clone https://bitbucket.org/manti_by/shcollage.git src && cd src/
        $ pip install -Ur app/requirements.txt

3. Build wed app

        $ make npm-install && make npm-build

4. Run local dev server

        $ make local


**NOTICE**: Before pushing your changes, run next commands

    $ make check
    $ make ci


## Run in production

To run app in production mode, clone repo, run docker and collect static

    $ git clone https://manti_by@bitbucket.org/manti_by/shcollage.git && cd shcollage/
    $ make npm-release && make start 
from django.db import models

from core.models import BaseModel


class CollageModel(BaseModel):
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title

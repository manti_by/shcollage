import graphene
from graphene_django import DjangoObjectType

from collage.models import CollageModel


class Collage(DjangoObjectType):
    class Meta:
        model = CollageModel


class Query(graphene.ObjectType):
    collages = graphene.List(Collage)

    def resolve_collages(self, info, **kwargs):
        return CollageModel.objects.all()

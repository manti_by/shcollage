import graphene
import graphql_jwt

import collage.schema
import user.schema

"""
This class will inherit from multiple Queries
as we begin to add more apps to our project
"""


class Query(user.schema.Query, collage.schema.Query, graphene.ObjectType):
    pass


class Mutation(user.schema.Mutation, graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)

from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from graphene_django.views import GraphQLView

from core import views as core_views
from core.schema import schema


urlpatterns = [
    url(r"^$", core_views.index, name="index"),
    url("graphql", GraphQLView.as_view(graphiql=True, schema=schema)),
    url(r"^dashboard/", admin.site.urls),
]


if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

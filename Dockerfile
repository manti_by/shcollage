# Use an official Python runtime as a parent image
FROM python:3.7-slim

# Install system packages
RUN apt-get update && \
    apt-get install -y --no-install-recommends gcc python3-dev

# Add directories
RUN mkdir -p /srv/shcollage/src/ && \
    mkdir -p /srv/shcollage/static/ && \
    mkdir -p /var/log/shcollage/

# Install any needed packages specified in requirements
COPY app/requirements.txt /tmp/requirements.txt
RUN pip install --trusted-host pypi.org --no-cache-dir --upgrade pip && \
    pip install --trusted-host pypi.org --no-cache-dir -r /tmp/requirements.txt

# Cleanup
RUN apt-get purge -y --auto-remove gcc python3-dev

# Run gunicorn
EXPOSE 8765
WORKDIR /srv/shcollage/src/app/
CMD exec gunicorn core.wsgi:application --bind 0.0.0.0:8765 --workers 2

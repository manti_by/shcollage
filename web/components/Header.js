import React, {Component} from 'react';
import {Nav, Navbar, NavDropdown} from 'react-bootstrap';

import Login from './Login';

class Header extends Component {
  render() {
    return (
      <header>
        <div className="container">
          <Navbar className="row" bg="light" expand="lg">
            <Navbar.Brand href="#home">{this.props.title}</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">&nbsp;</Nav>
              <NavDropdown title="Login" alignRight id="basic-nav-dropdown">
                <Login/>
              </NavDropdown>
            </Navbar.Collapse>
          </Navbar>
        </div>
      </header>
    );
  }
}

export default Header;
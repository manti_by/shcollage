import React, {Component} from 'react';

import gql from 'graphql-tag';
import {graphql} from 'react-apollo';

import Header from './Header';
import CollageList from './CollageList';

class App extends Component {
  render() {
    return (
      <>
        <Header title="SHcollage"/>
        <CollageList data={this.props.data}/>
      </>
    );
  }
}

const allCollagesQuery = gql`
  query {
    collages {
      uuid
      title
    }
  }
`;

const AppWithData = graphql(allCollagesQuery)(App);

export default AppWithData;
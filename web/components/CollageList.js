import React, {Component} from 'react';
import {Card} from 'react-bootstrap';

class CollageList extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <main>
        <div className="container">
          <div className="row">
            {this.props.data.loading === true ? "Loading" : this.props.data.collages.map(data =>
              <Card className="col-12 col-md-6 col-lg-3" key={data.uuid}>
                <Card.Img variant="bottom" data-src="holder.js/100px180"/>
                <Card.Body>
                  <Card.Title>{data.title}</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">{data.uuid}</Card.Subtitle>
                  <Card.Link href="#collage">Card Link</Card.Link>
                </Card.Body>
              </Card>
            )}
          </div>
        </div>
      </main>
    );
  }
}

export default CollageList;
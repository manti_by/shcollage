import React from "react";
import {Mutation} from "react-apollo";
import {Button, Form, FormGroup, FormControl, FormLabel} from "react-bootstrap";
import gql from "graphql-tag";


const LOGIN_MUTATION = gql`
  mutation login($email: String!, $password: String!) {
    tokenAuth(email: $email, password: $password) {
      token
    }
  }
`;


const Login = () => {

  let email, password;

  return (
    <Mutation mutation={LOGIN_MUTATION}>
      {(login, {data}) => (
        <div className="container">
          <Form onSubmit={e => {
            e.preventDefault();
            login({variables: {email: email.value, password: password.value}});
            email.value = "";
          }} className="login">
            <FormGroup controlId="email">
              <FormLabel>Email</FormLabel>
              <FormControl name="email" type="email" ref={node => {
                email = node;
              }} autoFocus/>
            </FormGroup>
            <FormGroup controlId="password">
              <FormLabel>Password</FormLabel>
              <FormControl name="password" type="password" ref={node => {
                password = node;
              }}/>
            </FormGroup>
            <Button block type="submit">
              Login
            </Button>
          </Form>
        </div>
      )}
    </Mutation>
  );
};

export default Login;
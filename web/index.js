import React from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie'

import {ApolloProvider} from 'react-apollo';
import {ApolloClient} from 'apollo-boost';
import {HttpLink} from 'apollo-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';

import App from './components/App';


const httpLink = {
  uri: '/graphql',
  headers: {
    "X-CSRFToken": Cookies.get('csrftoken'),
  }
};

const client = new ApolloClient({
  link: new HttpLink(httpLink),
  cache: new InMemoryCache()
});

ReactDOM.render(
  <ApolloProvider client={client}><App/></ApolloProvider>,
  document.getElementById('root')
);

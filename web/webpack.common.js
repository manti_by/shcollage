const webpack = require('webpack');

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      'Holder': 'holderjs',
      'holder': 'holderjs',
      'window.Holder': 'holderjs'
    })
  ]
};
local:
	python ./app/manage.py runserver 127.0.0.1:8765

migrate:
	python ./app/manage.py migrate

migration:
	python ./app/manage.py makemigrations

messages:
	python ./app/manage.py makemessages

compile-messages:
	python ./app/manage.py compilemessages

venv:
	virtualenv -p $(shell which python3) --no-site-packages --prompt='shc-' ../venv

pip:
	pip install -Ur app/requirements.txt

ci:
	circleci build

check:
	black -t py37 app/
	isort app/*.py
	flake8 app/


build:
	docker build -f Dockerfile -t mantiby/shcollage:latest .

build-test:
	docker build -f Dockerfile.test -t mantiby/shcollage:test .

start:
	docker-compose up -d

stop:
	docker-compose stop

destroy:
	docker-compose down

static:
	docker exec -it shcollage-app python manage.py collectstatic --no-input

bash:
	docker exec -it shcollage-app bash


npm-install:
	npm install --prefix web/

npm-build:
	npm run dev --prefix web/

npm-release:
	npm run prod --prefix web/
